CREATE TABLE "user" (
  "id" SERIAL NOT NULL PRIMARY KEY,
  "username" VARCHAR(255) NOT NULL UNIQUE,
  "pwd_hash" VARCHAR(255) NOT NULL
);

CREATE TABLE "follow" (
  "id" SERIAL NOT NULL PRIMARY KEY,
  "follower_id" INTEGER NOT NULL REFERENCES "user" ("id"),
  "followed_id" INTEGER NOT NULL REFERENCES "user" ("id"),
  UNIQUE("follower_id", "followed_id")
);

CREATE TABLE "post" (
  "id" SERIAL NOT NULL PRIMARY KEY,
  "user_id" INTEGER NOT NULL REFERENCES "user" ("id"),
  "content" VARCHAR(255) NOT NULL,
  "timestamp" timestamptz NOT NULL
);

CREATE TABLE "share" (
  "id" SERIAL NOT NULL PRIMARY KEY,
  "user_id" INTEGER NOT NULL REFERENCES "user" ("id"),
  "post_id" INTEGER NOT NULL REFERENCES "post" ("id"),
  UNIQUE("user_id", "post_id")
);

CREATE TABLE "mention" (
  "id" SERIAL NOT NULL PRIMARY KEY,
  "post_id" INTEGER NOT NULL REFERENCES "post" ("id"),
  "user_id" INTEGER NOT NULL REFERENCES "user" ("id"),
  UNIQUE("post_id", "user_id")
);

CREATE TABLE "tag" (
  "id" SERIAL NOT NULL PRIMARY KEY,
  "name" VARCHAR(255) NOT NULL
);

CREATE TABLE "post_tag" (
  "id" SERIAL NOT NULL PRIMARY KEY,
  "post_id" INTEGER NOT NULL REFERENCES "post" ("id"),
  "tag_id" INTEGER NOT NULL REFERENCES "tag" ("id"),
  UNIQUE("post_id", "tag_id")
);
