CREATE FUNCTION register("_username" VARCHAR(255), "password" VARCHAR(255))
RETURNS BOOLEAN AS $$
BEGIN
    IF NOT EXISTS(SELECT 1 from "user" where "user"."username" = "_username") THEN
        INSERT INTO "user" ("username", "pwd_hash") VALUES ("_username", "password");
        RETURN TRUE;
    END IF;
    RETURN FALSE;
END;
$$ LANGUAGE plpgsql;

CREATE FUNCTION authenticate("_username" VARCHAR(255), "password" VARCHAR(255))
RETURNS BOOLEAN AS $$
BEGIN
    RETURN EXISTS (SELECT 1 from "user" where "user"."username" = "_username" AND "pwd_hash" = "password");
END;
$$ LANGUAGE plpgsql;

CREATE FUNCTION post("user_id" INTEGER, "content" TEXT)
RETURNS BOOLEAN AS $$
BEGIN
    IF EXISTS(SELECT 1 from "user" where "id" = "user_id") THEN
        INSERT INTO "post" ("user_id", "content", "timestamp") VALUES ("user_id", "content", now());
        RETURN TRUE;
    END IF;
    RETURN FALSE;
END;
$$ LANGUAGE plpgsql;

CREATE FUNCTION follow("follower" INTEGER, "followed" INTEGER)
RETURNS BOOLEAN AS $$
BEGIN
    IF EXISTS(SELECT 1 from "user" where "id" = "follower") AND EXISTS(SELECT 1 from "user" where "id" = "followed") THEN
        IF NOT EXISTS(SELECT 1 from "follow" where "follower_id" = "follower" AND "followed_id" = "followed") THEN
            INSERT INTO "follow" ("follower_id", "followed_id") VALUES ("follower", "followed");
            RETURN TRUE;
        END IF;
    END IF;
    RETURN FALSE;
END;
$$ LANGUAGE plpgsql;
