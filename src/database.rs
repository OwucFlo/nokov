use diesel::*;

use std::env;

use schema::*;

mod sql_fn {
    use diesel::*;

    sql_function!(
        register,
        register_t,
        (username: types::VarChar, password: types::VarChar) -> types::Bool
    );

    sql_function!(
        authenticate,
        authenticate_t,
        (username: types::VarChar, password: types::VarChar) -> types::Bool
    );

    sql_function!(
        post,
        post_t,
        (username: types::Integer, content: types::Text) -> types::Bool
    );

    sql_function!(
        follow,
        follow_t,
        (followe: types::Integer, followed: types::Integer) -> types::Bool
    );
}

#[derive(Clone, Debug, Queryable)]
pub struct User {
    pub id: i32,
    pub username: String,
    pub pwd_hash: String
}

#[derive(Clone, Debug, Queryable)]
pub struct Post {
    pub id: i32,
    pub user_id: i32,
    pub content: String,
    pub timestamp: data_types::PgTimestamp
}

pub fn connect() -> PgConnection {
    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");

    PgConnection::establish(&database_url).expect(&format!("Error connecting to {}", database_url))
}

pub fn user_from_id(user_id: i32) -> Option<User> {

    if let Ok(users) = user::table.filter(user::id.eq(user_id)).load::<User>(&connect()) {
        if users.len() == 1 {
            return Some(users[0].clone());
        }
    }
    None
}

pub fn register(username: &str, password: &str) -> bool {
    select(sql_fn::register(username, password)).get_result(&connect()).unwrap()
}

pub fn authenticate(username: &str, password: &str) -> Option<i32> {
    let connection = connect();
    if select(sql_fn::authenticate(username, password)).get_result(&connection).unwrap() {
        Some(user::table.filter(user::username.eq(username)).load::<User>(&connection).unwrap()[0].id)
    } else { None }
}

pub fn post(user_id: i32, content: &str) -> bool {
    select(sql_fn::post(user_id, content)).get_result(&connect()).unwrap()
}

pub fn posts() -> Vec<Post> {
    post::table.load(&connect()).unwrap()
}

pub fn follow(follower: i32, followed: i32) -> bool {
    select(sql_fn::follow(follower, followed)).get_result(&connect()).unwrap()
}

pub fn following(user_id: i32) -> Vec<User> {
    let connection = connect();
    user::table
        .inner_join(follow::table.on(
            user::id.eq(follow::followed_id)
                .and(follow::follower_id.eq(user_id))
        ))
        .select((user::id, user::username, user::pwd_hash))
        .load(&connection).unwrap()
}
