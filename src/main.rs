#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_codegen;
extern crate dotenv;

#[macro_use]
extern crate iron;
extern crate iron_sessionstorage;
#[macro_use]
extern crate router;
extern crate urlencoded;
extern crate persistent;

mod html;
mod database;
mod models;
mod schema;

use dotenv::dotenv;

use iron::prelude::*;
use iron::status;
use iron::modifiers::Redirect;

use iron_sessionstorage::traits::*;
use iron_sessionstorage::SessionStorage;
use iron_sessionstorage::backends::SignedCookieBackend;

use urlencoded::UrlEncodedBody;

struct User(i32);

impl iron_sessionstorage::Value for User {
    fn get_key() -> &'static str { "logged_in_user" }
    fn into_raw(self) -> String { self.0.to_string() }
    fn from_raw(value: String) -> Option<Self> {
        if let Ok(user_id) = value.parse::<i32>() {
            Some(User(user_id))
        } else {
            None
        }
    }
}

/// entry point to web page
fn greet(req: &mut Request) -> IronResult<Response> {
    let html = if let Ok(Some(User(user_id))) = req.session().get::<User>() {
        html::page(&html::logged_in_page(user_id))
    } else {
        html::page(&format!("{}{}", html::LOGIN_FORM, html::REGISTRATION_FORM))
    };

    let content_type = "text/html".parse::<iron::mime::Mime>().unwrap();
    Ok(Response::with((status::Ok, content_type, html)))
}

fn register(req: &mut Request) -> IronResult<Response> {
    let (username, password) = {
        // get data from submitted form
        let formdata = iexpect!(req.get_ref::<UrlEncodedBody>().ok());
        (iexpect!(formdata.get("username"))[0].to_owned(),
         iexpect!(formdata.get("password"))[0].to_owned())
    };

    database::register(&username, &password);

    // redirect back to starting page
    Ok(Response::with((status::Found, Redirect(url_for!(req, "greet")))))
}

fn login(req: &mut Request) -> IronResult<Response> {
    let (username, password) = {
        // get data from submitted form
        let formdata = iexpect!(req.get_ref::<UrlEncodedBody>().ok());
        (iexpect!(formdata.get("username"))[0].to_owned(),
         iexpect!(formdata.get("password"))[0].to_owned())
    };

    if let Some(user_id) = database::authenticate(&username, &password) {
        req.session().set(User(user_id)).ok();
    }

    // redirect back to starting page
    Ok(Response::with((status::Found, Redirect(url_for!(req, "greet")))))
}

fn post(req: &mut Request) -> IronResult<Response> {
    let content = {
        let formdata = iexpect!(req.get_ref::<UrlEncodedBody>().ok());
        iexpect!(formdata.get("content"))[0].to_owned()
    };

    if let Ok(Some(User(user_id))) = req.session().get::<User>() {
        database::post(user_id, &content);
        Ok(Response::with((status::Found, Redirect(url_for!(req, "greet")))))
    } else {
        Ok(Response::with((status::Found, Redirect(url_for!(req, "greet")))))
    }
}

fn follow(req: &mut Request) -> IronResult<Response> {
    let followed = {
        let formdata = iexpect!(req.get_ref::<UrlEncodedBody>().ok());
        iexpect!(formdata.get("user_id"))[0].parse::<i32>().unwrap()
    };

    if let Ok(Some(User(follower))) = req.session().get::<User>() {
        database::follow(follower, followed);
    }

    Ok(Response::with((status::Found, Redirect(url_for!(req, "greet")))))
}

fn logout(req: &mut Request) -> IronResult<Response> {
    req.session().clear().ok();
    Ok(Response::with((status::Found, Redirect(url_for!(req, "greet")))))
}

fn main() {
    dotenv().ok();

    // associate pages with functions
    let router = router!(
        greet: get "/" => greet,
        register: post "/register" => register,
        login: post "/login" => login,
        post: post "/post" => post,
        follow: post "/follow" => follow,
        logout: get "/logout" => logout,
        logout_post: post "/logout" => logout
    );

    let my_secret = b"verysecret".to_vec();
    let mut ch = Chain::new(router);
    ch.link_around(SessionStorage::new(SignedCookieBackend::new(my_secret)));
    let _res = Iron::new(ch).http("localhost:1337");
    println!("Listening on port 1337.");
}
